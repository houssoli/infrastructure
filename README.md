# Infrastructure for Voter App

## Pre-Requisites

* Google API enabled for
  [Memorystore](https://console.developers.google.com/apis/api/redis.googleapis.com/overview)
  and [Cloud
  SQL](https://console.cloud.google.com/flows/enableapi?apiid=sqladmin)

* Terraform Cloud

  1. Sign up for an account at app.terraform.io
  1. [Create an
     organization.](https://www.terraform.io/docs/cloud/users-teams-organizations/organizations.html)
     This is the `TFC_ORGANIZATION` GitLab variable.
  1. Using the free tier, [create a Team API
     Token](https://app.terraform.io/app/hashicorp-team-demo/settings/teams) for
     the `owners` team. This is the `TFC_TOKEN` GitLab variable.
  1. Under the organization, create [a workspace without VCS
     connection](https://www.terraform.io/docs/cloud/workspaces/creating.html).
     This is the `TFC_WORKSPACE` GitLab variable.

* Publicly available Vault instance. You will need the `VAULT_ADDR` and
  `VAULT_TOKEN`.

  1. Bring your own OR
  1. Use the
     [vote-infrastructure-setup](https://gitlab.com/gitlab-com/alliances/hashicorp/sandbox-projects/voting-app/vote-infrastructure-setup)
     to create one.

## Environment Variables for Pipeline

1. `TFC_TOKEN`: Team API token for Terraform Cloud
1. `TFC_ORGANIZATION`: Team organization name from Terraform Cloud.
1. `TFC_WORKSPACE`: Name of the Terraform Cloud Workspace.
1. `VAULT_ADDR`: address to Vault instance.
1. `VAULT_TOKEN`: Vault token with access to write to generic secret

## Terraform Cloud Variables

Go to the Terraform Cloud workspace you created and select the "Variables" tab.
Type and completed the following variables for the workspace.

1. `project`: GCP project for lab

1. `region`: GCP region for lab

1. `password`: Password for Postgres database for admin

1. `zone`: GCP zone for Kubernetes cluster

1. `GOOGLE_CREDENTIALS`: JSON payload with service account key for
   infrastructure. Must have minimum access to Kubernetes, Cloud SQL, and
   MemoryStore as administrator. Flatten the credentials and remove newlines by issuing:

   ```shell 
   > cat <service account>.json | jq -c .
   ```

1. `CONFIRM_DESTROY`: Set to 1. Allows destruction of the infrastructure.