resource "google_redis_instance" "cache" {
  name           = "${var.app_prefix}-${var.environment}-cache"
  tier           = "BASIC"
  memory_size_gb = 1

  redis_version = var.redis_version == "3.2" ? "REDIS_3_2" : "REDIS_4_0"
  display_name  = "${var.app_prefix} ${var.environment} Instance"

  labels = {
    app         = var.app_prefix
    environment = var.environment
  }
}