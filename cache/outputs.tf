output "cache_host" {
  value       = google_redis_instance.cache.host
  description = "Host endpoint of cache"
}

output "cache_port" {
  value       = google_redis_instance.cache.port
  description = "Port of cache"
}