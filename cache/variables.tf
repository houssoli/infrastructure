variable "app_prefix" {
  type        = string
  description = "Application name"
}

variable "environment" {
  type        = string
  description = "Application environment"
}

variable "redis_version" {
  type        = string
  default     = "4.0"
  description = "Redis version must be 3.2 or 4.0"
}