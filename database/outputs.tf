output "connection" {
  value       = base64encode(google_sql_database_instance.instance.connection_name)
  description = "Connection name of database"
}

output "user" {
  value       = base64encode(google_sql_user.users.name)
  description = "Database user"
}

output "password" {
  value       = base64encode(google_sql_user.users.password)
  sensitive   = true
  description = "Database password"
}

output "credentials" {
  value = google_service_account_key.postgres.private_key
  sensitive   = true
  description = "Credentials for Cloud SQL proxy"
}