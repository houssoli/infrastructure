resource "google_sql_database_instance" "instance" {
  name             = "${var.app_prefix}-${var.environment}-database-instance"
  database_version = "POSTGRES_11"
  settings {
    tier = "db-f1-micro"
  }
}

resource "google_sql_database" "database" {
  depends_on = [google_sql_database_instance.instance]
  name       = "${var.app_prefix}-${var.environment}-database"
  instance   = google_sql_database_instance.instance.name
}

resource "google_sql_user" "users" {
  name     = google_sql_database.database.name
  instance = google_sql_database_instance.instance.name
  password = var.password
}