resource "google_service_account" "postgres" {
  account_id   = "${var.app_prefix}-${var.environment}-database"
  display_name = "${var.app_prefix}-${var.environment}-database"
}

resource "google_service_account_key" "postgres" {
  service_account_id = google_service_account.postgres.name
}

resource "google_project_iam_member" "project" {
  role   = "roles/cloudsql.client"
  member = "serviceAccount:${google_service_account.postgres.email}"
}