variable "app_prefix" {
  type        = string
  description = "Application name"
}

variable "environment" {
  type        = string
  description = "Application environment"
}

variable "password" {
  type        = string
  description = "Database password to use"
}