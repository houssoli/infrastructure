FROM registry.gitlab.com/gitlab-org/cluster-integration/auto-deploy-image:v0.8.3

RUN apk add jq

ENV version=1.2.2

ADD https://releases.hashicorp.com/vault/${version}/vault_${version}_linux_amd64.zip /tmp/vault.zip

RUN \
  cd /bin &&\
  unzip /tmp/vault.zip &&\
  chmod +x /bin/vault &&\
  rm /tmp/vault.zip