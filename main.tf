terraform {
  backend "remote" {
    hostname     = "app.terraform.io"
    organization = "TFC_ORGANIZATION"

    workspaces {
      name = "TFC_WORKSPACE"
    }
  }
}

provider "vault" {
  version         = "~> 2.8"
  skip_tls_verify = true
}

provider "google" {
  version = "~> 3.10"
  project = var.project
  zone    = var.zone
  region  = var.region
}

provider "kubernetes" {
  version = "~> 1.11.1"
}

locals {
  app           = "voter-app"
  environment   = "prod"
  k8s_namespace = "vote"
}

module "cache" {
  source      = "./cache"
  app_prefix  = local.app
  environment = local.environment
}

module "database" {
  source      = "./database"
  app_prefix  = local.app
  environment = local.environment
  password    = var.password
}
