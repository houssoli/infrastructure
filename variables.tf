variable "project" {
  type        = string
  description = "Google Project ID"
}

variable "zone" {
  type        = string
  description = "Google Zone"
}

variable "region" {
  type        = string
  description = "Google Region"
}

variable "password" {
  type        = string
  description = "Password for database"
}