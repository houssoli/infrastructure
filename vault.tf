locals {
  database_host = base64encode("127.0.0.1")
}

resource "vault_mount" "voter" {
  path        = "secret"
  type        = "generic"
  description = "Voter mount"
}

resource "vault_generic_secret" "voter_app_cache" {
  path       = "secret/voter-cache"
  data_json  = <<EOT
    {"kind":"ConfigMap","apiVersion":"v1","metadata":{"name":"redis"},"data":{"redis_host":"${module.cache.cache_host}"}}
  EOT
  depends_on = [vault_mount.voter]
}

resource "vault_generic_secret" "voter_app_cloudsql" {
  path       = "secret/voter-cloudsql"
  data_json  = <<EOT
    {"kind":"Secret","apiVersion":"v1","metadata":{"name":"cloudsql"},"data":{"credentials":"${module.database.credentials}"},"type":"Opaque"}
  EOT
  depends_on = [vault_mount.voter]
}

resource "vault_generic_secret" "voter_app_postgres" {
  path       = "secret/voter-postgres"
  data_json  = <<EOT
    {"kind":"Secret","apiVersion":"v1","metadata":{"name":"postgres"},"data":{"connection":"${module.database.connection}","host":"${local.database_host}","password":"${module.database.password}","username":"${module.database.user}"},"type":"Opaque"}
  EOT
  depends_on = [vault_mount.voter]
}